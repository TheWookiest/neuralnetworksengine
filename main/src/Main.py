import numpy as np
from time import time
from nn.layers import InputLayer
from nn.layers import HiddenLayer
from nn import Network
from utils import Serializer
from utils import PreparedDataset
from utils.datasets.PictureImitation import PictureImitation
from utils.list.ListUtils import ListUtils
from utils import Graphics
from functions import ActivationFunctions

t0 = time()

# activationFunction = ActivationFunctions.reLu
# activationFunctionDerivative = ActivationFunctions.derivativeReLu
#
# inputLayerConfig = 3
# hiddenLayerConfig = [2]
# outputLayerConfig = 1
#
# nn = Network(inputLayerConfig, hiddenLayerConfig, outputLayerConfig)
# print("Creation:::")
# # nn.print()
# # print("end creation--------------")
#
# # print("After:")
# nn.setWeights(PreparedDataset.testWeightSet)
# nn.print()
# print("end creation--------------")
#
# dataset = PreparedDataset.testData
# nn.train(activationFunction, activationFunctionDerivative, dataset, epoch=5000, learningRate=0.1)
# print("++++++++ END TEST ++++++++++++++++")
# nn.print()
# Graphics.show(nn.accuracy)
#
# print("+++++++++ RESULT ++++++++++++++++++")
# for data in dataset:
#     print("--")
#     print("Test data: " + str(data[0]))
#     result = nn.predict(activationFunction, np.array(data[0]))
#     print("Predicted: " + str(result))
#     print("Actual: " + str(data[1]))
#     print("--")




# ============== TRAIN 2 =========================
activationFunction = ActivationFunctions.sigmoid
activationFunctionDerivative = ActivationFunctions.derivativeSigmoid

dataset2 = PictureImitation.bigTrainData

inputLayerConfig = len(dataset2[0][0])
hiddenLayerConfig = [len(dataset2[0][0]), len(dataset2[0][0]), len(dataset2[0][0]), len(dataset2[0][0])]
outputLayerConfig = len(dataset2[0][1])

nn = Network(inputLayerConfig, hiddenLayerConfig, outputLayerConfig)
print("Creation:::")

# nn.train(activationFunction, activationFunctionDerivative, dataset2, epoch=20000, learningRate=0.01)
# Serializer.save(nn)
#
# print("")
# print("After Training::::::::::::")
# nn.print()
#
# print("")
# print("+++++++++ RESULT ++++++++++++++++++")
# for data in dataset2:
#     print("--")
#     print("Test data: " + str(data[0]))
#     result = nn.predict(activationFunction, np.array(data[0]))
#     print("Predicted: " + str(result))
#     print("Actual: " + str(data[1]))
#     print("--")
#
# Graphics.show(nn.accuracy)

# ============== TEST 2 =========================

loaded = Serializer.load("save-2018-08-08 16:45:35.542912.p")
print(str(loaded))

nn.setWeights(loaded)
for data in PictureImitation.bigTestData:
    print("--")
    print("Test data: " + str(data[0]))
    result = nn.predict(activationFunction, np.array(data[0]))
    print("Predicted: " + str(result))
    print("Actual: " + str(data[1]))
    print("--")


print("TIME IS : " + str(time() - t0))



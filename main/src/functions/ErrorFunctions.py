import numpy as np


class ErrorFucntions():

    @staticmethod
    def MSE(expected, real):
        return np.mean((np.array(expected) - np.array(real))**2)

    @staticmethod
    def signedError(expected, real):
        # print("EXPECTDDDD: " + str(expected))
        # print("REALLLLLLLL: " + str(real))
        # print("SIGNED_MSE :::::::+++++++" + str(np.array(expected) - np.array(real)))
        return (np.array(expected) - np.array(real))*abs(np.array(expected) - np.array(real))

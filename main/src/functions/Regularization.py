from functions import ErrorFucntions

class Regularization():

    @staticmethod
    def __calculateL2__(layer):
        l2 = 0
        i = 0
        for neuron in layer.neurons:
            l2 += neuron.inputWeights[i]**2
            i += 1

        return l2


    @staticmethod
    def signedErrorWithL2(expected, real, layer, lambdaL2):
        return ErrorFucntions.signedError(expected, real) + lambdaL2*Regularization.__calculateL2__(layer)



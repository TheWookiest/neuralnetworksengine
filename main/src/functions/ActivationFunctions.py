import math


class ActivationFunctions:

    @staticmethod
    def justX(x):
        return x

    @staticmethod
    def test(x):
        if x < 0.5:
            return 0
        else:
            return 1

    @staticmethod
    def sigmoid(x):
        return 1/(1 + math.exp(-x))

    @staticmethod
    def derivativeSigmoid(x):
        return ActivationFunctions.sigmoid(x)*(1 - ActivationFunctions.sigmoid(x))

    @staticmethod
    def reLu(x):
        return max(0, x)

    @staticmethod
    def derivativeReLu(x):
        if x <= 0:
            return 0

        return 1

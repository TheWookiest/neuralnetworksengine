
class PreparedDataset(object):

    testWeightSet = [
        [[1], [1], [1]],
        [[0.79, 0.44, 0.43], [0.85, 0.43, 0.29]],
        [[0.5, 0.52]]
    ]
    
    testData = [
        ([1, 1, 0], [0]),
        ([0, 0, 0], [0]),
        ([0, 0, 1], [1]),
        ([0, 1, 0], [0]),
        ([0, 1, 1], [0]),
        ([1, 0, 0], [1]),
        ([1, 0, 1], [1]),
        ([1, 1, 1], [1])
    ]

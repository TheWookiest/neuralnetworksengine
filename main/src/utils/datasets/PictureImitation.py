import numpy as np


class PictureImitation(object):
    trainData = np.array([
        (
            [
                1, 1, 1, 1, 1,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                1, 1, 1, 1, 1,
                1, 0, 0, 0, 1,
                1, 0, 0, 0, 1,
                1, 0, 0, 0, 1,
                1, 1, 1, 1, 1
            ],
            [0, 1, 0]
        ),
        (
            [
                1, 0, 0, 0, 1,
                0, 1, 0, 1, 0,
                0, 0, 1, 0, 0,
                0, 1, 0, 1, 0,
                1, 0, 0, 0, 1
            ],
            [0, 0, 1]
        )

    ])

    testData = np.array([
        (
            [
                1, 1, 1, 1, 1,
                0, 0, 1, 0, 0,
                0, 0, 1, 1, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                1, 1, 1, 1, 1,
                0, 1, 1, 0, 1,
                0, 0, 1, 0, 0,
                0, 0, 0, 1, 0,
                1, 1, 0, 1, 1
            ],
            [1, 0, 0]
        ),
        (
            [
                1, 1, 1, 1, 1,
                0, 0, 1, 0, 0,
                0, 1, 1, 1, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0
            ],
            [1, 0, 0]
        )
    ])

    bigTrainData = np.array([
        (
            [
                0, 0, 0, 0, 0, 0, 0,
                0, 1, 1, 1, 1, 1, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 0, 0, 0, 0, 0,
                1, 1, 1, 1, 1, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 1, 1, 1, 1, 1, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 1, 1, 1, 1, 1,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 0, 1, 1, 1, 1,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 1, 1, 1, 1, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 1, 1, 1, 1, 1,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 0, 0, 0, 0, 0,
                0, 1, 1, 1, 1, 1, 0,
                0, 1, 0, 0, 0, 1, 0,
                0, 1, 0, 0, 0, 1, 0,
                0, 1, 0, 0, 0, 1, 0,
                0, 1, 1, 1, 1, 1, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [0, 1, 0]
        ),
        (
            [
                0, 1, 1, 1, 1, 1, 0,
                0, 1, 0, 0, 0, 1, 0,
                0, 1, 0, 0, 0, 1, 0,
                0, 1, 0, 0, 0, 1, 0,
                0, 1, 0, 0, 0, 1, 0,
                0, 1, 1, 1, 1, 1, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [0, 1, 0]
        ),
        (
            [
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 1, 1, 1, 1, 1,
                0, 0, 1, 0, 0, 0, 1,
                0, 0, 1, 0, 0, 0, 1,
                0, 0, 1, 0, 0, 0, 1,
                0, 0, 1, 1, 1, 1, 1
            ],
            [0, 1, 0]
        ),
        (
            [
                1, 1, 1, 1, 1, 0, 0,
                1, 0, 0, 0, 1, 0, 0,
                1, 0, 0, 0, 1, 0, 0,
                1, 0, 0, 0, 1, 0, 0,
                1, 1, 1, 1, 1, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [0, 1, 0]
        ),
        (
            [
                0, 0, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 1, 0,
                0, 0, 1, 0, 1, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 1, 0, 1, 0, 0,
                0, 1, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [0, 0, 1]
        ),
        (
            [
                1, 0, 0, 0, 0, 0, 1,
                0, 1, 0, 0, 0, 1, 0,
                0, 0, 1, 0, 1, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 1, 0, 1, 0, 0,
                0, 1, 0, 0, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 1
            ],
            [0, 0, 1]
        )

    ])

    bigTestData = np.array([
        (
            [
                0, 0, 0, 0, 0, 0, 0,
                0, 1, 1, 1, 1, 1, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 1, 1, 1, 1, 1, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 1, 1, 1, 1, 1,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 0, 0, 0, 0, 0,
                0, 1, 1, 1, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                1, 1, 1, 0, 1, 0, 0,
                1, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 1, 1, 1, 1, 1,
                0, 0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 1, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 0, 1, 1, 1, 1,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        ),
        (
            [
                0, 0, 0, 1, 1, 1, 1,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 0, 1, 0, 0,
                0, 0, 0, 1, 1, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0
            ],
            [1, 0, 0]
        )

    ])

import numpy as np

class ListUtils(object):

    @staticmethod
    def matrixToList(matrix):
        # return list(chain.from_iterable([[1, 2, 3], [1, 2], [1, 4, 5, 6, 7]]))
        return sum(matrix, [])


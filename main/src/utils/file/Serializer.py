import pickle
import datetime


class Serializer(object):
    folderToSavePath = "/home/olexii/Documents/nn/saves/"

    @staticmethod
    def save(nn, fileToSave="save-" + str(datetime.datetime.now())):
        toSave = []

        for layer in nn.layers:
            tempLayer = []
            for neuron in layer.neurons:
                tempLayer.append(neuron.inputWeights)
            toSave.append(tempLayer)

        pickle.dump(toSave, open(Serializer.folderToSavePath + fileToSave + ".p", "wb"),
                    protocol=pickle.HIGHEST_PROTOCOL)

        with open(Serializer.folderToSavePath + fileToSave + ".txt", "w") as text_file:
            for layer in nn.layers:
                print("[{}];".format(len(layer.neurons)), file=text_file)

    @staticmethod
    def load(fileName):
        return pickle.load(open(Serializer.folderToSavePath + fileName, "rb"))

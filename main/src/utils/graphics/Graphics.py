import matplotlib.pylab as plt


class Graphics():

    @staticmethod
    def show(plots):
        plt.plot(plots)  # Построение графика

        plt.xlabel(r'$Epoch$')  # Метка по оси x в формате TeX
        plt.ylabel(r'$Error$')  # Метка по оси y в формате TeX
        plt.title(r'$y=Accuracy$')  # Заголовок в формате TeX
        plt.grid(True)  # Сетка

        plt.show()  # Показать график
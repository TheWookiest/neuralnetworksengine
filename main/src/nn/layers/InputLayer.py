import random
from nn.neurons.Neuron import Neuron
# from nn.connections.Connection import Connection


class InputLayer(object):
    neurons = []

    def __init__(self, neuronsInLayer=2):
        # self.neurons = [Neuron(random.uniform(-1.0, 1.0)) for c in range(count)]
        self.layerId = "input"
        for c in range(neuronsInLayer):
            self.neurons.append(Neuron(c, 1))

    """
        inputValues should be numpy.array()
    """
    def calculateOutputs(self, inputValues, activationFunction):
        i = 0
        for n in self.neurons:
            n.outputValue = inputValues[i]
            i += 1

        # self.print()

    def print(self):
        print("== Input Layer ====================================")
        for n in self.neurons:
            n.print()
        print("===================================================")

    def printWeights(self):
        print("== Input Layer ===================================")
        for n in self.neurons:
            n.printWeights()
        print("===================================================")


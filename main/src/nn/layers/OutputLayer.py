import random
import numpy as np
from nn.neurons.Neuron import Neuron
from nn.layers.InputLayer import InputLayer
from functions.ActivationFunctions import ActivationFunctions


""" May contain result function. """


class OutputLayer(object):
    neurons = []

    def __init__(self, previousLayerNeuronsCount, neuronsInLayer=1):
        # self.neurons = [Neuron(random.uniform(-1.0, 1.0)) for c in range(count)]
        for c in range(neuronsInLayer):
            self.neurons.append(Neuron(c, [random.uniform(-1.0, 1.0)
                                           for n in range(previousLayerNeuronsCount)]))

    """
       inputValues should be numpy.array()
    """
    def calculateOutputs(self, inputValues, activationFunction):
        for n in self.neurons:
            n.disactivatedValue = np.sum(n.inputWeights*inputValues)
            n.outputValue = activationFunction(n.disactivatedValue)

        # =============== Softmax function ================
        outputValuesSum = 0
        for n in self.neurons:
            outputValuesSum += n.outputValue
        for n in self.neurons:
            n.outputValue = n.outputValue/outputValuesSum
        # ===============================



        # self.print()

    def print(self):
        print("== Output Layer ===================================")
        for n in self.neurons:
            n.print()
        print("===================================================")

    def printWeights(self):
        print("== Output Layer ===================================")
        for n in self.neurons:
            n.printWeights()
        print("===================================================")

import random
import numpy as np
from nn.neurons.Neuron import Neuron
from nn.layers.InputLayer import InputLayer
from functions.ActivationFunctions import ActivationFunctions


class HiddenLayer(object):
    neurons = []

    def __init__(self, layerId, previousLayerNeuronsCount, neuronsInLayer=2):
        self.layerId = layerId
        neurons = [Neuron(c, [random.uniform(-1.0, 1.0)
                                       for n in range(previousLayerNeuronsCount)])
                                for c in range(neuronsInLayer)]

        self.neurons = neurons

    """
       inputValues should be numpy.array()
    """
    def calculateOutputs(self, inputValues, activationFunction):

        for n in self.neurons:
            # print("Input values: " + str(inputValues))
            # print("Input weights: " + str(n.inputWeights))
            # print("Input weights fuuuuuuuu: " + str(n.inputWeights))
            # print("Result matrix: " + str(n.inputWeights*inputValues))
            # print("Sum of result matrix: " + str(np.sum(n.inputWeights*inputValues)))
            # print("Activation: " + str(ActivationFunctions.sigmoid(np.sum(n.inputWeights*inputValues))))

            n.disactivatedValue = np.sum(n.inputWeights * inputValues)
            n.outputValue = activationFunction(n.disactivatedValue)
            # print("n.out: " + str(n.outputValue))

        # self.print()

    def print(self):
        print("== Hidden Layer #" + str(self.layerId) + " ================")
        for n in self.neurons:
            n.print()
        print("===================================================")

    def printWeights(self):
        print("== Hidden Layer #" + str(self.layerId) + " ================")
        for n in self.neurons:
            n.printWeights()
        print("===================================================")

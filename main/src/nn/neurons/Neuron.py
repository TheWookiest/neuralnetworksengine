import numpy as np


class Neuron(object):
    inputWeights = np.array([])
    disactivatedValue = 0  # Not activated output value (multiplication of input values end input waights array)
    outputValue = 0  # Activated output (Activation function on disactivated value)

    def __init__(self, neuronId, inputWeights):
        self.neuronId = neuronId
        self.inputWeights = np.array(inputWeights)

    def print(self):
        print("-- Neuron #" + str(self.neuronId) + " --")
        print("Neuron input weights: ")
        print(str(self.inputWeights))
        print("Neuron disactivated value: " + str(self.disactivatedValue))
        print("Neuron output value: " + str(self.outputValue))
        print("----------------")

    def printWeights(self):
        print("-- Neuron #" + str(self.neuronId) + " --")
        print(str(self.inputWeights))
        print("----------------")

from functions import ActivationFunctions
from functions import ErrorFucntions
from functions import Regularization
import numpy as np


class BackPropagation():
    """
                Back propagation

            Output Layer:
            error = actual - expected

            Hidden Layer:
            error = weight * weights_delta

            1. weights_delta = error * activationFunc(x)dx
    """
    @staticmethod
    def trainEpoch(activationFunctionDerivative, data, learningRate, layers):
            print("====== TRAINING EPOCH START ==============")

            outputLayer = len(layers) - 1

            # Simple Error function
            # outputError = np.array([(layers[outputLayer].neurons[i].outputValue - data[1][i])*abs((layers[outputLayer].neurons[i].outputValue - data[1][i]))
            #                     for i in range(len(layers[outputLayer].neurons))])

            # Signed MSE
            # outputError = np.array([ErrorFucntions.signedError(layers[outputLayer].neurons[i].outputValue, data[1][i])
            #                         for i in range(len(layers[outputLayer].neurons))])

            # Signed MSE with regularization
            outputError = np.array([Regularization.signedErrorWithL2(layers[outputLayer].neurons[i].outputValue, data[1][i], layers[outputLayer], lambdaL2=0.1)
                                    for i in range(len(layers[outputLayer].neurons))])

            currentLayer = outputLayer
            while(currentLayer >= 1):
                # print("LAYER # " + str(currentLayer))
                # print("outputError : " + str(outputError))
                weightsDelta = np.array([outputError[i]
                                               * activationFunctionDerivative(
                                  layers[currentLayer].neurons[i].disactivatedValue)
                              for i in range(len(layers[currentLayer].neurons))])
                # weightsDelta = []
                # for i in range(len(self.layers[currentLayer].neurons)):
                #     weightsDelta.append(outputError[i]
                #                                * ActivationFunctions.derivativeSigmoid(
                #                   self.layers[currentLayer].neurons[i].disactivatedValue))

                # print("weightsDelta : " + str(weightsDelta))

                # print("Before weights")
                # self.layers[currentLayer].print()
                i = 0
                for currentNeuron in layers[currentLayer].neurons:
                    j = 0  # index for outputNeuron input weights
                    for prevNeuron in layers[currentLayer - 1].neurons:
                        # for delta in weightsDelta:

                            # print("--=---" + str(i) + str(j))
                            # print("For weights : " + str(currentNeuron.inputWeights))
                            # print("Current weight : " + str(currentNeuron.inputWeights[j]))
                            # print("Delta: " + str(weightsDelta[i]))
                            # print("old inputWeight : " + str(currentNeuron.inputWeights))
                            # print("--=---")

                            currentNeuron.inputWeights[j] = currentNeuron.inputWeights[j] - \
                                                   prevNeuron.outputValue*weightsDelta[i]*learningRate

                            # print("New inputWeight ::: " + str(currentNeuron.inputWeights))
                            j += 1
                    i += 1

                # print(" After weights::::::::::::::::;")
                # layers[currentLayer].printWeights()
                # print(" :::::::::::::::::::::::::::::;")

                outputError = []
                i = 0  # index for inputWeights of current Neurons
                for prevNeuron in layers[currentLayer - 1].neurons:
                    j = 0  # index for weightsDelta of current Neuron
                    tempError = 0
                    for n in layers[currentLayer].neurons:
                        # print("n.inputWeights : " + str(n.inputWeights[i]))
                        # print("weightsDelta : " + str(weightsDelta[j]))

                        tempError += n.inputWeights[i]*weightsDelta[j]
                        j += 1

                    outputError.append(tempError)

                    i += 1
                # outputError = np.array(outputError)

                # print("New Errors:: " + str(outputError))

                currentLayer -= 1

            print("========== END TRAINING ==============")
            return layers
import numpy as np
import pickle

from nn.layers.InputLayer import InputLayer
from nn.layers.HiddenLayer import HiddenLayer
from nn.layers.OutputLayer import OutputLayer
from functions.ActivationFunctions import ActivationFunctions
from nn.training.BackPropagation import BackPropagation
from functions.ErrorFunctions import ErrorFucntions


class Network(object):

    layers = []
    accuracy = []

    """
    inputLayerConfig:
    {count of neurons in hidden layer}
    
    hiddenLayersConfig:
    [..., {count of neurons in hidden layer #n}, ...]

    outputLayerConfig:
    {count of neurons in output layer}
    """
    def __init__(self, inputLayerConfig, hiddenLayersConfig, outputLayerConfig):
        self.layers.append(InputLayer(inputLayerConfig))

        i = 0  # index of layer in network
        for countOfNeuronsInHiddenLayer in hiddenLayersConfig:
            n = HiddenLayer(i+1, len(self.layers[i].neurons), countOfNeuronsInHiddenLayer)
            self.layers.append(n)

            i += 1

        self.layers.append(OutputLayer(len(self.layers[i].neurons), outputLayerConfig))

    """
    'weights' should looks like:
     [
      [[0.466], [0.654], [-0.334]]  -- input layer
      [[0.825, -0.929, -0.785], [-0.436, -0.632, -0.784]]  -- hidden layer
      ...
      [[0.241, 0.726]]  -- output layer
     ]
    
    """
    def setWeights(self, weights):
        l = 0  # layers index (in 'weights')
        for layer in self.layers:
            n = 0  # neuron index (in 'weights')
            for neuron in layer.neurons:
                neuron.inputWeights = weights[l][n]

                n += 1

            l += 1

    def predict(self, activationFunction, inputs):
        for layer in self.layers:
            layer.calculateOutputs(inputs, activationFunction)
            # print("INPUTS: ")
            # layer.print()
            inputs = np.array([n.outputValue for n in layer.neurons])

        outputLayer = len(self.layers) - 1
        return [n.outputValue for n in self.layers[outputLayer].neurons]

    def train(self, activationFunction, activationFunctionDerivative, dataset, epoch=5000, learningRate=0.1):
        for i in range(epoch):
            print("EPOCH : " + str(i))

            error = 0
            for data in dataset:
                predicted = self.predict(activationFunction, data[0])

                # print("ERROR: -")
                error += ErrorFucntions.MSE(data[1], predicted)
                # print(error)
                # print("-------------")

                self.layers = BackPropagation.trainEpoch(activationFunctionDerivative, data, learningRate, self.layers)

            print("ERROR:::::::::::::::::" + str(error))
            print("DATASET:::::::::::::::::" + str(len(dataset)))
            print("MEAN ERROR:::::::::::::::::" + str(error/len(dataset)))
            self.accuracy.append(error/len(dataset))

    def print(self):
        for l in self.layers:
            l.print()

